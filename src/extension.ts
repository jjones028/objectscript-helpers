// The module 'vscode' contains the VS Code extensibility API
// Import the module and reference it with the alias vscode in your code below
import * as vscode from "vscode";

import { AtelierApi } from "./api";

// this method is called when your extension is activated
// your extension is activated the very first time the command is executed
export function activate(context: vscode.ExtensionContext) {
  // Use the console to output diagnostic information (console.log) and errors (console.error)
  // This line of code will only be executed once when your extension is activated
  console.log(
    'Congratulations, your extension "objectscript-helpers" is now active!'
  );

  // The command has been defined in the package.json file
  // Now provide the implementation of the command with registerCommand
  // The commandId parameter must match the command field in package.json
  const api: AtelierApi = new AtelierApi();

  let disposable = vscode.commands.registerCommand(
    "extension.objectscript-helpers.importSource",
    async () => {
      let response = await api.serverInfo();

      console.log(response);

      response = await api.getDocNames("LYNK");

      const documents: any[] = response.filter((document: any) => {
        switch (document.db) {
          case "CACHELIB":
          case "IRISLIB":
          case "CACHESYS":
          case "IRISSYS":
            return false;
          default:
            return true;
        }
      });

      console.log(documents);

      response = await api.getDoc("LYNK", "dbo.LynkImport.cls");

      console.log(response);

      // The code you place here will be executed every time your command is executed

      // Display a message box to the user
      vscode.window.showInformationMessage("Importing from Source");
    }
  );

  context.subscriptions.push(disposable);
}

// this method is called when your extension is deactivated
export function deactivate() {}
