import * as vscode from "vscode";
import * as http from "http";
import { resolve } from "url";

export class AtelierApi {
  cookies: string[] = [];

  console = vscode.window.createOutputChannel("ObjectScript");

  outputConsole(lines: string[]): void {
    for (let line of lines) {
      this.console.appendLine(line);
    }
  }

  request(url: string): Promise<any> {
    return new Promise((resolve, reject) => {
      const req: http.ClientRequest = http.request(
        {
          auth: "_SYSTEM:SYS",
          method: "GET",
          host: "localhost",
          port: 80,
          path: "/api/atelier" + url,
          headers: {
            Cookie: this.cookies
          }
        },
        (response: http.IncomingMessage) => {
          this.cookies =
            response.headers["set-cookie"] !== undefined
              ? [...response.headers["set-cookie"]]
              : [];
          let body: string = "";

          response.on("data", chunk => (body += chunk));

          response.on("end", () => {
            let json = JSON.parse(body);

            if (json.console.length > 0) {
              this.outputConsole(json.result.console);
            }

            if (json.result.status) {
              reject(new Error(json.result.status));
              return;
            }

            resolve(json.result.content);
          });

          response.on("error", e => {
            console.log(e);
          });
        }
      );

      req.end();
    });
  }

  serverInfo(): Promise<any> {
    return this.request("/");
  }

  getDocNames(namespace: string): Promise<any> {
    return this.request("/v2/" + namespace + "/docnames");
  }

  getDoc(namespace: string, name: string): Promise<any> {
    return this.request("/v2/" + namespace + "/doc/" + name);
  }
}
